namespace Uebung_05
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdZeichen = new System.Windows.Forms.Button();
            this.cmdArray = new System.Windows.Forms.Button();
            this.cmdEnde = new System.Windows.Forms.Button();
            this.txtAusgabe = new System.Windows.Forms.TextBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmdZeichen
            // 
            this.cmdZeichen.Location = new System.Drawing.Point(431, 12);
            this.cmdZeichen.Name = "cmdZeichen";
            this.cmdZeichen.Size = new System.Drawing.Size(75, 23);
            this.cmdZeichen.TabIndex = 0;
            this.cmdZeichen.Text = "Zeichen";
            this.cmdZeichen.UseVisualStyleBackColor = true;
            this.cmdZeichen.Click += new System.EventHandler(this.cmdZeichen_Click);
            // 
            // cmdArray
            // 
            this.cmdArray.Location = new System.Drawing.Point(431, 41);
            this.cmdArray.Name = "cmdArray";
            this.cmdArray.Size = new System.Drawing.Size(75, 23);
            this.cmdArray.TabIndex = 1;
            this.cmdArray.Text = "Array";
            this.cmdArray.UseVisualStyleBackColor = true;
            this.cmdArray.Click += new System.EventHandler(this.cmdArray_Click);
            // 
            // cmdEnde
            // 
            this.cmdEnde.Location = new System.Drawing.Point(431, 92);
            this.cmdEnde.Name = "cmdEnde";
            this.cmdEnde.Size = new System.Drawing.Size(75, 23);
            this.cmdEnde.TabIndex = 2;
            this.cmdEnde.Text = "Ende";
            this.cmdEnde.UseVisualStyleBackColor = true;
            this.cmdEnde.Click += new System.EventHandler(this.cmdEnde_Click);
            // 
            // txtAusgabe
            // 
            this.txtAusgabe.Location = new System.Drawing.Point(13, 13);
            this.txtAusgabe.Multiline = true;
            this.txtAusgabe.Name = "txtAusgabe";
            this.txtAusgabe.Size = new System.Drawing.Size(351, 122);
            this.txtAusgabe.TabIndex = 3;
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(12, 153);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(351, 122);
            this.txtStatus.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 318);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.txtAusgabe);
            this.Controls.Add(this.cmdEnde);
            this.Controls.Add(this.cmdArray);
            this.Controls.Add(this.cmdZeichen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdZeichen;
        private System.Windows.Forms.Button cmdArray;
        private System.Windows.Forms.Button cmdEnde;
        private System.Windows.Forms.TextBox txtAusgabe;
        private System.Windows.Forms.TextBox txtStatus;
    }
}

