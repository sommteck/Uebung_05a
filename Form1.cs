	using System;
// using System.Collections.Generic;
// using System.ComponentModel;
// using System.Data;
// using System.Drawing;
using System.IO;  // Assembly für Massenspeicher
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uebung_05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdEnde_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmdZeichen_Click(object sender, EventArgs e)
        {
            // Array deklarieren
            byte[] zeichen = new byte[26] { 65, 66, 67, 68, 69, 70, 71, 72,
                                            73, 74, 75, 76, 77, 78, 79, 80,
                                            81, 82, 83, 84, 85, 86, 87, 88, 89, 90 };

            // FileStream deklarieren
            FileStream fs = new FileStream("C:\\Temp\\Test.txt", FileMode.Create);

            // Array in Datei schreiben
            for (int i = 0; i < 26; i++)
            {
                fs.WriteByte(zeichen[i]);
                txtAusgabe.AppendText(Convert.ToString((char)zeichen[i]));
            }

            // Zeilenvorschub in Textfeld
            txtAusgabe.AppendText("\r\n");

            // Datei schließen
            fs.Close();

            // Meldung der Fertigstellung ausgeben
            txtStatus.Text = "Zeichenweise Ausgabe ist fertig!";

        }

        private void cmdArray_Click(object sender, EventArgs e)
        {
            // Array deklarieren
            byte[] zeichen = new byte[26] { 65, 66, 67, 68, 69, 70, 71, 72, 73,
                                            74, 75, 76, 77, 78, 79, 80, 81, 82,
                                            83, 84, 85, 86, 87, 88, 89, 90 };

            // Filestream
            FileStream fs = new FileStream("C:\\Temp\\Test.txt", FileMode.Append);

            // Zeilenvorschub in Datei schreiben
            fs.WriteByte(13);
            fs.WriteByte(10);

            // Array in Datei schreiben
            fs.Write(zeichen, 0, 26);

            // Array in Textfeld ausgeben
            foreach (byte z in zeichen)
                txtAusgabe.AppendText(Convert.ToString((char)z));
            txtAusgabe.AppendText("\r\n");

            // Datei schließen
            fs.Close();

            // Meldung der Ferigstellung ausgeben
            txtStatus.Text = "Ausgabe des Arrays ist fertig!";

        }
    }
}
